

### technology stack

- java
    - concurrency support (blocking_queue, parallel_stream, completable_future)
- assertj - to test in more declarative way
- docker


### design overview
![img.png](img.png)

### how to run
- run from docker local image

0. clone git repository 
1. run `./mvnw clean install`
2. run `docker build -t fare-estimator .` to create an image
3. run `docker run -v ${directory_path_with_source_file}:/app fare-estimator /app/${file_name}`

example:
```shell
./mvnw clean install && docker build -t fare-estimator .
docker run -v /usr/projects/fare-estimator/resources:/app fare-estimator /app/paths.csv

```

- run from docker remote image

link : https://hub.docker.com/r/slisnychyi/fare-estimator

example:
```shell
docker pull slisnychyi/fare-estimator
docker run -v /usr/projects/fare-estimator/resources:/app slisnychyi/fare-estimator /app/paths.csv

```

### furthest improvements
- we may create abstract adapters for input and result source. 
  With an idea to work not only with files, but other sources.

Also, it was interesting to try a non-blocking, async approach for solving this task.
For example, the [project reactor](http://projectreactor.io/) library as an implementation of [reactive_streams](https://www.reactive-streams.org/)
can be used.

Test version of how such solution implementation may look like, but without backpressure for now.
- link: https://gitlab.com/slisnychyi/fare-estimator-async
  
  
  