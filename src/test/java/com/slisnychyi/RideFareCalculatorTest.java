package com.slisnychyi;

import com.slisnychyi.model.RideSegment;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class RideFareCalculatorTest {
    /**
     * Ride segments with different start and finish time. That represents the following options of fare calculation:
     * 1. Time of day (05:00, 00:00] - DAY_FARE
     * 2. Time of day (00:00, 05:00] - NIGHT_FARE
     * 3. Speed <= 10km/h - IDLE_FARE
     * 4. Start (05:00, 00:00] and Finish (00:00, 05:00] - DAY_FARE + NIGHT_FARE
     * 5. Start (00:00, 05:00] and Finish (05:00, 00:00] - NIGHT_FARE + DAY_FARE
     *
     * @return Stream of ride segments
     */
    private static Stream<Arguments> rideSegments() {
        return Stream.of(
                Arguments.of(generateSegment(LocalDateTime.parse("2022-04-18T05:00:10"), LocalDateTime.parse("2022-04-18T05:00:20"),
                        new BigDecimal("26.41"), new BigDecimal("0.14"), new BigDecimal("0.0053")), new BigDecimal("0.1036")),
                Arguments.of(generateSegment(LocalDateTime.parse("2022-04-18T00:00:30"), LocalDateTime.parse("2022-04-18T00:01:10"),
                        new BigDecimal("20.75"), new BigDecimal("0.11"), new BigDecimal("0.0053")), new BigDecimal("0.1430")),
                Arguments.of(generateSegment(LocalDateTime.parse("2022-04-18T00:06:00"), LocalDateTime.parse("2022-04-18T00:06:10"),
                        new BigDecimal("9.12"), new BigDecimal("0.14"), new BigDecimal("0.0053")), new BigDecimal("0.0631")),
                Arguments.of(generateSegment(LocalDateTime.parse("2022-04-18T23:59:49"), LocalDateTime.parse("2022-04-19T00:00:10"),
                        new BigDecimal("42.10"), new BigDecimal("0.24"), new BigDecimal("0.0053")), new BigDecimal("0.2472")),
                Arguments.of(generateSegment(LocalDateTime.parse("2022-04-18T04:58:50"), LocalDateTime.parse("2022-04-18T05:00:10"),
                        new BigDecimal("42.10"), new BigDecimal("0.24"), new BigDecimal("0.0053")), new BigDecimal("0.7578"))
        );
    }

    private static RideSegment generateSegment(LocalDateTime start, LocalDateTime finish, BigDecimal speed, BigDecimal distance, BigDecimal time) {
        return new RideSegment(start.toInstant(ZoneOffset.UTC).getEpochSecond(), finish.toInstant(ZoneOffset.UTC).getEpochSecond(),
                time, distance, speed);
    }

    @ParameterizedTest
    @MethodSource("rideSegments")
    public void should_calculateRideFare_with_differentValidRideSegments(RideSegment segment, BigDecimal expectedFare) {
        //given
        RideFareCalculator fareCalculator = new RideFareCalculator();
        //when
        BigDecimal result = fareCalculator.calculateFare(segment).setScale(4, RoundingMode.HALF_EVEN);
        //then
        assertThat(result).isEqualTo(expectedFare);
    }

}