package com.slisnychyi.util;

import org.junit.jupiter.api.Test;

import java.math.RoundingMode;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

class RideSegmentUtilsTest {

    @Test
    public void should_convertSecondsToHours() {
        //given
        long seconds = 60 * 60;
        //when
        double res = RideSegmentUtils.secondsToHours(seconds)
                .setScale(5, RoundingMode.HALF_EVEN).doubleValue();
        //then
        assertThat(res).isEqualTo(1);
    }

    @Test
    public void should_getException_when_secondsLessThenZero() {
        //given
        long seconds = -1;
        //when
        assertThatThrownBy(() -> RideSegmentUtils.secondsToHours(seconds))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("unable to transform seconds[-1] to hours");
    }


}