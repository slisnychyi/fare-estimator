package com.slisnychyi;

import com.slisnychyi.model.Tuple;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FareEstimatorTest {
    private static final String SOURCE_PATH = Paths.get("").toAbsolutePath() + "/src/test/resources/paths.csv";
    private static final String RESULT_PATH = Paths.get("").toAbsolutePath() + "/src/test/resources/result.csv";
    private static final String LARGE_SOURCE_PATH = Paths.get("").toAbsolutePath() + "/src/test/resources/large_paths.csv";

    @Test
    public void should_calculateRideFares_forSourceFileWithRide() throws IOException {
        //given
        String[] args = {SOURCE_PATH, RESULT_PATH};
        //when
        FareEstimator.main(args);
        Map<Integer, String> result = Files.lines(Paths.get(RESULT_PATH))
                .map(e -> e.split(","))
                .collect(Collectors.toMap(e -> Integer.parseInt(e[0]), e -> e[1].trim()));
        //then
        Map<Integer, String> expected = Map.of(4, "3.47", 8, "7.30", 6, "6.37", 9, "4.76", 5, "19.57",
                1, "7.82", 2, "11.47", 7, "19.81", 3, "19.71");
        assertThat(result.size()).isEqualTo(expected.size());
        expected.forEach((key, value) -> assertThat(result.get(key)).isEqualTo(value));
        //after
        Files.delete(Paths.get(RESULT_PATH));
    }

    @Test
    public void should_calculateRideFares_forSourceFileWithLargeFileSize() throws IOException {
        //given
        String[] args = {LARGE_SOURCE_PATH, RESULT_PATH};
        Files.createFile(Paths.get(LARGE_SOURCE_PATH));
        for (int i = 1; i < 2; i++) {
            generateFakeRides(Paths.get(SOURCE_PATH), Paths.get(LARGE_SOURCE_PATH), i);
        }
        //when
        FareEstimator.main(args);
        //then
        Files.delete(Paths.get(LARGE_SOURCE_PATH));
        Files.delete(Paths.get(RESULT_PATH));
    }

    @Test
    public void should_throwAnException_when_noSourceFileExists() {
        //given
        String[] args = {"/some/source/file.csv"};
        //when
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> FareEstimator.main(args));
        assertThat(exception.getMessage()).isEqualTo("No source file exists = " + args[0]);
    }

    private void generateFakeRides(Path sourcePath, Path resultPath, int i) throws IOException {
        Map<Integer, List<Tuple>> collect = Files.lines(sourcePath)
                .map(Tuple::generate)
                .collect(Collectors.groupingBy(Tuple::id));
        Map<Integer, List<Tuple>> map = new HashMap<>();
        int j = (i + 1) * 100_000;
        i = i * 100_000;
        while (i < j) {
            map.put(i, collect.get(ThreadLocalRandom.current().nextInt(1, 10)));
            i++;
        }
        List<String> resultLines = map.entrySet().stream()
                .flatMap(k -> k.getValue().stream()
                        .map(e -> k.getKey() + "," + e.lat() + "," + e.lon() + "," + e.timestamp()))
                .collect(Collectors.toList());
        Files.write(resultPath, resultLines, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
    }
}