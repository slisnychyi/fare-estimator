package com.slisnychyi;

import com.slisnychyi.model.Tuple;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class RideProcessorTest {
    private static final String RESULT_PATH = Paths.get("").toAbsolutePath() + "/src/test/resources/result.csv";

    @AfterEach
    public void setup() throws IOException {
        Files.delete(Paths.get(RESULT_PATH));
    }

    @Test
    public void should_processRideFare_when_queueHasMessages() throws InterruptedException, IOException {
        //given
        BlockingQueue<Map<Integer, List<Tuple>>> blockingQueue = new ArrayBlockingQueue<>(10);
        blockingQueue.put(Map.of(1, List.of(
                Tuple.generate("1,37.867469,23.753909,1405589293"),
                Tuple.generate("1,37.867469,23.753909,1405589302"),
                Tuple.generate("1,37.867515,23.753706,1405589312"),
                Tuple.generate("1,37.867732,23.752911,1405589324")
        )));
        //when
        new RideProcessor(Paths.get(RESULT_PATH), blockingQueue, new RideFareCalculator()).processRides();
        Map<Integer, String> result = Files.lines(Paths.get(RESULT_PATH))
                .map(e -> e.split(","))
                .collect(Collectors.toMap(e -> Integer.parseInt(e[0]), e -> e[1].trim()));
        //then
        assertThat(result.get(1)).isEqualTo("3.47");
    }


}