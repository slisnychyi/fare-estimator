package com.slisnychyi;

import com.slisnychyi.model.Tuple;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class RideReaderTest {

    @Test
    public void should_storeInputRides_toTheBlocingQueue() throws InterruptedException {
        //given
        Path INPUT_SOURCE = Paths.get(Paths.get("").toAbsolutePath() + "/src/test/resources/paths.csv");
        BlockingQueue<Map<Integer, List<Tuple>>> blockingQueue = new ArrayBlockingQueue<>(10);
        RideReader reader = new RideReader(INPUT_SOURCE, blockingQueue);
        //when
        reader.populateRides();
        //then
        Map<Integer, List<Tuple>> result = blockingQueue.poll(100, TimeUnit.MILLISECONDS);
        assertThat(result).isNotNull();
        assertThat(result.get(1).size()).isEqualTo(132);
    }


}