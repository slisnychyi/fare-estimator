package com.slisnychyi;

import com.slisnychyi.model.RideSegment;
import com.slisnychyi.model.Tuple;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public record RideProcessor(Path resultPath,
                            BlockingQueue<Map<Integer, List<Tuple>>> queue,
                            RideFareCalculator fareCalculator) {
    private static final int MAX_SEGMENT_SPEED = 100;
    private static final BigDecimal DEFAULT_FARE = new BigDecimal("3.47");

    /**
     * Processing rides data by reading messages(ride tuples grouped by id) from blocking queue {@link RideProcessor#queue}
     * and running them in parallel.
     * Calculating ride fare for ride segments {@link RideSegment} and constructing ride fare line and store to the result file.
     */
    public void processRides() {
        try (BufferedWriter writer = Files.newBufferedWriter(resultPath)) {
            Map<Integer, List<Tuple>> rideTuplesById = queue.poll(1, TimeUnit.SECONDS);
            while (rideTuplesById != null) {
                rideTuplesById.entrySet().stream()
                        .parallel()
                        .map(e -> createRideFareLine(e.getKey(), e.getValue()))
                        .forEach(value -> write(writer, value));
                writer.flush();
                rideTuplesById = queue.poll(500, TimeUnit.MILLISECONDS);
            }
        } catch (IOException | InterruptedException e) {
            throw new IllegalArgumentException("unable to process rides for = " + resultPath, e);
        }
    }

    private void write(BufferedWriter writer, String value) {
        try {
            writer.write(value);
        } catch (IOException e) {
            System.out.printf("unable to write value. cause = %s", e.getMessage());
        }
    }

    private String createRideFareLine(int rideId, List<Tuple> tuples) {
        BigDecimal fare = BigDecimal.ZERO;
        for (int i = 1; i < tuples.size(); i++) {
            Tuple prev = tuples.get(i - 1);
            Tuple cur = tuples.get(i);
            if (cur.timestamp() != prev.timestamp()) {
                RideSegment segment = RideSegment.generate(prev, cur);
                if (segment.speed().doubleValue() <= MAX_SEGMENT_SPEED) {
                    fare = fare.add(fareCalculator.calculateFare(segment));
                }
            }
        }
        fare = DEFAULT_FARE.doubleValue() > fare.doubleValue() ? DEFAULT_FARE : fare.setScale(2, RoundingMode.HALF_EVEN);
        return String.format("%s, %s\n", rideId, fare);
    }
}
