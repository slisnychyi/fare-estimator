package com.slisnychyi;

import com.slisnychyi.model.Tuple;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

public record RideReader(Path sourcePath,
                         BlockingQueue<Map<Integer, List<Tuple>>> queue) {
    private static final int DEFAULT_BUFFER_SIZE = 1000;

    /**
     * The input data is guaranteed to contain continuous row blocks for each individual ride (i.e. the data will not be multiplexed).
     * In addition, the data is also pre-sorted for you in ascending timestamp order.
     * We read ride data from input source, transform ride lines to the {@link Tuple} and aggregate by id.
     * When the number of aggregated data becomes >= {@value DEFAULT_BUFFER_SIZE} we send them to the blocking queue {@link RideReader#queue}.
     */
    public void populateRides() {
        try (BufferedReader reader = Files.newBufferedReader(sourcePath)) {
            Map<Integer, List<Tuple>> tuples = new ConcurrentHashMap<>();
            while (reader.ready()) {
                Tuple tuple = Tuple.generate(reader.readLine());
                if (tuples.containsKey(tuple.id())) {
                    tuples.get(tuple.id()).add(tuple);
                } else {
                    if (tuples.size() >= DEFAULT_BUFFER_SIZE) {
                        queue.put(tuples);
                        tuples = new ConcurrentHashMap<>();
                    }
                    tuples.put(tuple.id(), new ArrayList<>(List.of(tuple)));
                }
            }
            if (!tuples.isEmpty()) {
                queue.put(tuples);
            }
        } catch (IOException | InterruptedException e) {
            System.out.printf("Unable to process rides. Cause = [%s]", e);
        }
    }
}
