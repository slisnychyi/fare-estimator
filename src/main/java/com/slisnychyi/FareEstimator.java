package com.slisnychyi;

import com.slisnychyi.model.Tuple;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;

public class FareEstimator {
    private static final int DEFAULT_QUEUE_SIZE = 10;
    private static final BlockingQueue<Map<Integer, List<Tuple>>> BLOCKING_QUEUE = new ArrayBlockingQueue<>(DEFAULT_QUEUE_SIZE);

    /**
     * {@link FareEstimator#main(String[])} starts {@link RideReader} in separate thread with sourcePath.
     * Try to take source file from current directory = ./source.csv, if sourcePath is not provided as input argument.
     * Also starts {@link RideProcessor} with {@link RideFareCalculator}, {@link FareEstimator#BLOCKING_QUEUE} and
     * resultPath. Try to create a result file in the current directory = ./result.csv, if resultPath is not provided as input argument.
     *
     * @param args (optional) source path with rides data and (optional) result path.
     * @throws IOException If no source path exists
     */
    public static void main(String[] args) throws IOException {
        if (args.length < 1) throw new IllegalArgumentException("source file argument was missed.");
        Path sourcePath = Paths.get(args[0]);
        if (sourcePath.toFile().exists()) {
            Path resultPath = Paths.get(sourcePath.getParent() + "/result.csv");

            Instant start = Instant.now();
            System.out.printf("Start processing rides [from=%s, size=%s bytes]\n", sourcePath, Files.size(sourcePath));

            CompletableFuture.runAsync(() -> {
                RideReader rideReader = new RideReader(sourcePath, BLOCKING_QUEUE);
                rideReader.populateRides();
            });

            CompletableFuture.runAsync(() -> {
                RideFareCalculator fareCalculator = new RideFareCalculator();
                RideProcessor rideProcessor = new RideProcessor(resultPath, BLOCKING_QUEUE, fareCalculator);
                rideProcessor.processRides();
            }).join();

            System.out.printf("Result was persisted [to=%s, in=%s]\n", resultPath, Duration.between(start, Instant.now()));
        } else {
            throw new IllegalArgumentException("No source file exists = " + sourcePath);
        }
    }
}
