FROM openjdk:17-oracle
COPY target/*.jar fare-estimator.jar
ENTRYPOINT ["java","-jar","/fare-estimator.jar"]